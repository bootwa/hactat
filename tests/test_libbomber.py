from hypothesis import given
import hypothesis.strategies as st
import pytest

from libbomber import Field, Wall, Bomb, Brick, Player, LEFT, RIGHT, UP, DOWN

from .utils import mk_field, ALL_DIRS


def test_empty_field():
    field = Field(10, 10)
    assert field.width == 10
    assert field.height == 10
    for x in range(10):
        for y in range(10):
            assert field.get_items((x, y)) == []


def test_place_items():
    field = mk_field(' W o ',
                     ' # @ |')

    def check(pos, cls):
        items = field.get_items(pos)
        assert len(items) == 1
        assert isinstance(items[0], cls)

    check((1, 0), Wall)
    check((3, 0), Bomb)
    check((1, 1), Brick)
    check((3, 1), Player)


@pytest.mark.parametrize('init_field,moves,expect_coords', [
    ((' W @ ',), [LEFT, 1], (2.8, 0)),   # One step.
    ((' W @ ',), [LEFT, 6], (2, 0)),     # Collision.
    # Choose one free direction out of two.
    (('@W',
      '  '), [DOWN, RIGHT, 1], (0, 0.2)),
    (('@ ',
      'W '), [DOWN, RIGHT, 1], (0.2, 0)),
    # Move sideways to enable movement in chosen direction.
    (('@W',
      '  '), [DOWN, 3, RIGHT, 3], (0.4, 1)),
    ((' W',
      ' @'), [LEFT, 3, UP, 3], (0, 0.6)),
    (('@ ',
      ' W'), [DOWN, 2, RIGHT, 3], (0.4, 0)),
    (('@ ',
      ' W'), [RIGHT, 2, DOWN, 3], (0, 0.4)),
    # Fix missed turn.
    (('@  ',
      'W W'), [RIGHT, 6, DOWN, 2], (1, 0.4)),
])
def test_move(init_field, moves, expect_coords):
    """Check that the controls are interpreted in a smart manner"""
    field = mk_field(*init_field)
    player = field.play_moves(moves)
    assert player.pos == expect_coords


@given(st.lists(st.sampled_from(ALL_DIRS + list(range(20)))))
def test_random_running(moves):
    """Check that random running is handled in a sane way"""
    field = mk_field('     ',
                     ' W@W ',
                     '     ')
    field.play_moves(moves)


def test_bomb_explode():
    field = mk_field('     ',
                     '  oW ',
                     '  #  ',
                     '     ')
    field.tick()
    assert field == mk_field('  *  ',
                             '***W ',
                             '  *  ',
                             '     ')
    field.ticks(5)
    assert field == mk_field('     ',
                             '   W ',
                             '     ',
                             '     ')


def test_crossdetonate():
    field = mk_field('o  o ',
                     '# o  ',
                     ' W o ')
    for item in field.items:
        if isinstance(item, Bomb) and item.pos != (0, 0):
            item.target_t = 1

    field.tick()
    assert field == mk_field('*****',
                             '* o* ',
                             ' W***')


def test_place_bomb():
    field = mk_field('@ ')
    player = field.items[0]
    player.set_desired_directions([RIGHT])
    field.tick()
    player.try_to_place_bomb()
    field.ticks(4)
    assert field == mk_field('o@')
    player.try_to_place_bomb()  # Fail: only one bomb.
    field.tick()
    assert field == mk_field('o@')


def test_place_bomb_same_place():
    field = mk_field('@  ')
    player = field.items[0]
    player.set_max_bomb_count(2)
    player.set_desired_directions([RIGHT])
    player.try_to_place_bomb()
    field.tick()
    player.try_to_place_bomb()
    field.ticks(4)
    player.try_to_place_bomb()
    field.ticks(5)
    assert field == mk_field('oo@')
