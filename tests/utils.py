from libbomber import (Field, Wall, Brick, Player, Bomb, Explosion,
                       LEFT, RIGHT, UP, DOWN)


ITEM_CLASSES = {
    'W': Wall,
    '#': Brick,
    '@': Player,
    'o': Bomb,
    '*': Explosion,
}

ALL_DIRS = [LEFT, RIGHT, UP, DOWN]


class TestField(Field):
    """Field that can compare itself to another one for testing"""

    def ticks(self, number):
        """Run the game for `number` of ticks"""
        for i in range(number):
            self.tick()

    def sorted_items(self):
        """Return all items sorted by position and class"""
        return sorted(self.items, key=lambda i: i.pos + (i.__class__,))

    def play_moves(self, moves):
        """Execute a sequence of moves, verifying some constraints"""
        player = [i for i in self.items if isinstance(i, Player)][0]
        desired_directions = []

        for cmd in moves:
            if cmd in ALL_DIRS:
                if cmd in desired_directions:
                    desired_directions.remove(cmd)
                else:
                    desired_directions.append(cmd)
                player.set_desired_directions(desired_directions)
            else:
                self.ticks(cmd)
                desired_directions = []

            x, y = player.pos
            # Check that we're within the bounds...
            assert 0 <= x <= self.width
            assert 0 <= y <= self.height
            # ...and one coordinate is integer.
            assert int(x) == x or int(y) == y

        return player

    def __eq__(self, other):
        """Compare the items and their positions both fields"""
        if len(self.items) != len(other.items):
            return False

        for s_i, o_i in zip(self.sorted_items(), other.sorted_items()):
            if s_i.pos != o_i.pos or s_i.__class__ != o_i.__class__:
                return False

        return True

    def __repr__(self):
        return '<Field: {}>'.format(', '.join(repr(i)
                                    for i in self.sorted_items()))


def mk_field(*rows):
    """Make a test field from strings representing objects in each row"""
    field = TestField(len(rows[0]), len(rows))
    for y, row in enumerate(rows):
        for x, cell in enumerate(row):
            if cell in ITEM_CLASSES:
                item_class = ITEM_CLASSES[cell]
                field.add_item(item_class(), (x, y))
    return field
