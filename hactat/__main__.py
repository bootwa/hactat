from hactat import render

import libbomber

ITEM_CLASSES = {
    'W': libbomber.Wall,
    '#': libbomber.Brick,
    '@': libbomber.Player,
    'o': lambda: libbomber.Bomb(3),
}


def mk_field(data):
    rows = [row.strip().strip('|') for row in data.splitlines()]
    field = libbomber.Field(
        len(rows[0]),
        len(rows),
        ticks_per_second=60,
        pixels_per_unit=32)
    for y, row in enumerate(rows):
        for x, cell in enumerate(row):
            if cell in ITEM_CLASSES:
                item_class = ITEM_CLASSES[cell]
                field.add_item(item_class(), (x, y))
    return field


def main():
    field = mk_field('''|WWWWWW|
                        |W@   W|
                        |W WWWW|
                        |W WW W|
                        |WWW o |
                        |WWWW W|''')

    world = render.World(field)
    world.loop()


if __name__ == '__main__':
    main()
