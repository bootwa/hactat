import configparser
import pathlib


def parse_ini():
    hactat = pathlib.Path(__file__).parent
    root = (hactat / '..').resolve()
    ini = hactat / 'hactat.ini'

    values = {'root': root.as_posix()}

    config = configparser.ConfigParser(values)
    with ini.open() as fp:
        config.read_file(fp)

    return config
