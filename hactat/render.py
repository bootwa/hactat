import pygame
import pyganim

from hactat import config

import libbomber

conf = config.parse_ini()


class Stop(Exception):
    pass


class Tile(pygame.sprite.Sprite):
    def __init__(self, world, image, pos=None, size=None, *args, **kwargs):

        self.world = world

        if pos is None:
            pos = (0, 0)
        if size is None:
            size = (self.world.tile_width, self.world.tile_height)

        self.cart_rect = pygame.rect.Rect(pos, size)

        self.image = image

        self.rect = self.image.get_rect()

        super(Tile, self).__init__(*args, **kwargs)

    def update(self, *args, **kwargs):
        cx, cy = self.cart_rect.topleft

        ix = (cx - cy) * 1
        iy = (cx + cy) * 1 / 2

        offset = 3.5*self.rect.width  # offset x to fit all tiles on screen
        ix += offset

        self.rect.topleft = ix, iy

        return super().update(*args, **kwargs)


class AnimatedTile(Tile):
    def __init__(self, images, world, *args, **kwargs):
        frames = list(zip(images, [100] * len(images)))
        self.anim = pyganim.PygAnimation(frames)
        image = pygame.surface.Surface(
            (world.image_width, world.image_height),
            pygame.SRCALPHA)
        super(AnimatedTile, self).__init__(
            image=image, world=world, *args, **kwargs)

    def control_animation(self):
        pass

    def update(self, *args, **kwargs):
        self.control_animation()

        # render first sprite if stationary otherwise animate
        if self.anim._state == pyganim.STOPPED:
            self.image.blit(self.anim.getFrame(0), (0, 0))
        else:
            self.anim.blit(self.image, (0, 0))

        return super().update(*args, **kwargs)


class WalkingTile(AnimatedTile):
    orientation = (1, 0)

    def __init__(self, images, *args, **kwargs):
        self.animation_images = []

        for i in range(0, len(images), 3):
            self.animation_images.append(images[i:i+3])

        super().__init__(images=self.animation_images[0], *args, **kwargs)

    def control_animation(self, *args, **kwargs):
        # switch animation sprites according to orientation
        if self.orientation != self.item.speed:
            self.orientation = self.item.speed
            ox, oy = self.orientation
            if ox > 0:
                self.anim._images = self.animation_images[0]
            elif ox < 0:
                self.anim._images = self.animation_images[1]
            elif oy > 0 and len(self.animation_images) == 4:
                self.anim._images = self.animation_images[2]
            elif oy < 0 and len(self.animation_images) == 4:
                self.anim._images = self.animation_images[3]
            self.image.fill((0, 0, 0, 0))

        # move tile to new position
        ix, iy = self.item.pos

        ix = int(ix * self.world.tile_width)
        iy = int(iy * self.world.tile_width)

        if (ix, iy) != self.cart_rect.topleft:
            self.cart_rect.topleft = (ix, iy)
            self.anim.play()
        else:
            self.anim.stop()

        return super().control_animation(*args, **kwargs)


class ItemTile(Tile):
    def __init__(self, item, *args, **kwargs):
        self.item = item
        super().__init__(*args, **kwargs)


class FloorTile(Tile):
    def __init__(self, *args, **kwargs):
        image = pygame.image.load(conf['assets']['floor_tile']).convert_alpha()
        super().__init__(image=image, *args, **kwargs)


class WallTile(ItemTile, Tile):
    def __init__(self, *args, **kwargs):
        image = pygame.image.load(conf['assets']['wall_tile']).convert_alpha()
        super().__init__(image=image, *args, **kwargs)


class PlayerTile(ItemTile, WalkingTile):
    def __init__(self, world, *args, **kwargs):
        rects = []

        # XXX: getImagesFromSpriteSheet with width/height is broken so we
        # have to do it ourselves until it's fixed.
        sheetImage = pygame.image.load(conf['assets']['player'])

        width = world.image_width
        height = world.image_height

        y_stop = sheetImage.get_height()
        y_step = sheetImage.get_height() // (sheetImage.get_height() // height)

        x_stop = sheetImage.get_width()
        x_step = sheetImage.get_width() // (sheetImage.get_width() // width)

        for y in range(0, y_stop, y_step):
            if y + height > sheetImage.get_height():
                continue
            for x in range(0, x_stop, x_step):
                if x + width > sheetImage.get_width():
                    continue
                rects.append((x, y, width, height))

        images = pyganim.getImagesFromSpriteSheet(
            filename=conf['assets']['player'],
            rects=rects,
        )

        super().__init__(images=images, world=world, *args, **kwargs)


class BombTile(ItemTile, AnimatedTile):
    def __init__(self, *args, **kwargs):
        images = pyganim.getImagesFromSpriteSheet(
            filename=conf['assets']['bomb'],
            rows=1,
            cols=3,
            rects=[],
        )

        super(BombTile, self).__init__(images=images, *args, **kwargs)

        self.anim.play()


class ExplosionTile(ItemTile, AnimatedTile):
    def __init__(self, *args, **kwargs):
        images = pyganim.getImagesFromSpriteSheet(
            filename=conf['assets']['explosion'],
            rows=1,
            cols=3,
            rects=[],
        )
        super().__init__(images=images, *args, **kwargs)

        self.anim.play()


class BoardTiles(pygame.sprite.OrderedUpdates):
    def __init__(self, world, *args, **kwargs):
        super(BoardTiles, self).__init__(*args, **kwargs)

        self.world = world

        width = 1 + int(max(f.pos[0] for f in self.world.field.items))
        height = 1 + int(max(f.pos[1] for f in self.world.field.items))

        # Create floor tiles to fit all
        for x in range(width):
            for y in range(height):
                tile = FloorTile(
                    world=world,
                    pos=(x*world.tile_width, y*world.tile_height))
                self.add(tile)

    def update(self, *args, **kwargs):
        # ignore tiles that are not represented on the field e.g. floor tiles
        tiles = [s for s in self.sprites() if hasattr(s, 'item')]

        # remove tiles that are no longer on the field
        for tile in tiles:
            found = False
            for item in self.world.field.items:
                if tile.item == item:
                    found = True
                    break
            else:
                tile.kill()

        # add tile for items that have been added to the field
        for item in self.world.field.items:
            found = False
            for tile in tiles:
                if tile.item == item:
                    found = True
                    break
            if not found:
                x, y = item.pos
                tile = self.world.get_tile_for_item(
                    item=item,
                    world=self.world,
                    pos=(x*self.world.tile_width, y*self.world.tile_height))
                self.add(tile)

        return super().update(*args, **kwargs)


class World:
    screen_width = int(conf['render']['screen_width'])
    screen_height = int(conf['render']['screen_height'])

    tile_width = int(conf['render']['tile_width'])
    tile_height = int(conf['render']['tile_height'])

    image_width = int(conf['render']['image_width'])
    image_height = int(conf['render']['image_height'])

    item_to_tile = {
        libbomber.Player: PlayerTile,
        libbomber.Bomb: BombTile,
        libbomber.Wall: WallTile,
        libbomber.Explosion: ExplosionTile,
    }

    def __init__(self, field, *args, **kwargs):
        pygame.init()

        self.screen = pygame.display.set_mode(
            (self.screen_width, self.screen_height))

        self.field = field

        super().__init__(*args, **kwargs)

    def get_tile_for_item(self, item, *args, **kwargs):
        TileKlass = self.item_to_tile[type(item)]
        return TileKlass(item=item, *args, **kwargs)

    def loop(self):
        clock = pygame.time.Clock()
        self.screen.fill((0, 0, 0))

        board_tiles = BoardTiles(world=self)

        for item in self.field.items:
            if item.__class__.__name__ == 'Player':
                player = item
                break
        else:
            raise ValueError('No player found')

        try:
            direction = []
            arrow_keys = {
                pygame.K_LEFT: libbomber.LEFT,
                pygame.K_RIGHT: libbomber.RIGHT,
                pygame.K_UP: libbomber.UP,
                pygame.K_DOWN: libbomber.DOWN,
            }

            while True:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        raise Stop
                    elif event.type == pygame.KEYDOWN:
                        if event.key in arrow_keys:
                            direction.append(arrow_keys[event.key])
                        elif event.key == pygame.K_SPACE:
                            player.try_to_place_bomb()
                        elif event.key == pygame.K_ESCAPE or \
                                event.key == pygame.K_q:
                            raise Stop
                    elif event.type == pygame.KEYUP:
                        if event.key in arrow_keys:
                            direction.pop(
                                direction.index(arrow_keys[event.key]))

                player.set_desired_directions(direction)

                self.screen.fill((0, 0, 0))

                self.field.tick()

                board_tiles.update()
                board_tiles.draw(self.screen)
                pygame.display.flip()

                clock.tick(60)  # limit to 60 FPS (frames per second)
        except Stop:
            pass
        except KeyboardInterrupt:
            pass
        finally:
            pygame.quit()
