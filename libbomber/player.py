from .utils import vmul, vadd, vsub, vnorm, vturn
from .items import ActiveItem, MovingItem, Bomb


# Directions in which a player can go.
LEFT = 'Left'
RIGHT = 'Right'
UP = 'Up'
DOWN = 'Down'

# Initial speed of players
INIT_SPEED = 2

# Delay of the bomb
BOMB_DELAY = 3


class Player(ActiveItem, MovingItem):
    """Human player"""

    max_speed = INIT_SPEED
    max_bomb_count = 1
    bombs_left = 1

    desired_directions = set()  # type: set[int]
    want_to_place_bomb = False

    def set_max_bomb_count(self, max_count):
        diff = max_count - self.max_bomb_count
        self.max_bomb_count = max_count
        self.bombs_left += diff

    def set_desired_directions(self, directions):
        """Set the directions in which the player 'wants' to move"""
        self.desired_directions = directions

    def try_to_place_bomb(self):
        """Indicate that the player would like to place a bomb"""
        self.want_to_place_bomb = True

    def _try_to_place_bomb(self):
        """Try to place a bomb on the field"""
        pos = self.field.quantize_pos(self.pos, to_tile=True)
        items = self.field.get_items(pos)
        if self.bombs_left > 0:
            for item in items:
                if isinstance(item, Bomb):
                    break
            else:
                self.field.add_item(Bomb(self.field.time + BOMB_DELAY), pos)
                self.bombs_left -= 1

    def _adjust_speed(self, speed):
        """Adjust speed so that movement is not blocked"""
        if speed == (0, 0):
            return speed

        def try_speed(speed, pos=self.pos):
            new_pos = self.field._move_tick(pos, speed)
            return self.field._item_can_move_to(self, new_pos)

        if try_speed(speed):
            return speed

        left = vturn(speed, -1)
        if try_speed(left):
            return left

        right = vturn(speed, 1)
        if try_speed(right):
            return right

        fix_pos = self.field.quantize_pos(self.pos, to_tile=True)
        if try_speed(speed, fix_pos):
            return vmul(vnorm(vsub(fix_pos, self.pos)), self.max_speed)

        return (0, 0)

    def _update_speed(self):
        sd = (0, 0)
        for dd in self.desired_directions:
            sd = vadd(sd, {
                LEFT: (-1, 0),
                RIGHT: (1, 0),
                UP: (0, -1),
                DOWN: (0, 1)
            }[dd])
        self.speed = self._adjust_speed(vmul(sd, self.max_speed))

    def update(self, t):
        self._update_speed()
        super().update(t)  # This will move the player.
        if self.want_to_place_bomb:
            self._try_to_place_bomb()
            self.want_to_place_bomb = False
