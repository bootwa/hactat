from .utils import vadd, vmul, in_bounds


class Item:
    """Generic item base class"""

    field = None  # type: Field
    pos = None    # type: tuple[int, int]
    blocking = True

    def __repr__(self):
        return '<{} at {}>'.format(self.__class__.__name__, self.pos)


class ActiveItem(Item):
    """Item that can react to events and passing of time"""

    def update(self, t):
        """Update this item

        Parameters:
        - t -- time from start of the game
        """
        s = super()
        if hasattr(s, 'update'):
            s.update(t)


class MovingItem(Item):
    """Items that move according to their speed"""

    speed = (0, 0)
    blocking = False  # Moving items don't block


class Bomb(ActiveItem, MovingItem):
    """Bomb placed onto the field"""

    EXPLOSION_TIME = 0.3  # How long does the explosion last?
    EXPLOSION_RADIUS = 3  # How far does the explosion spread

    def __init__(self, target_t=0):
        """The bomb will explode at `target_t`"""
        self.target_t = target_t

    def update(self, t):
        if t >= self.target_t:
            self.explode()

    def explode(self):
        """Explode this bomb"""
        field = self.field
        origin = self.pos
        field.remove_item(self)

        def explode_tile(pos):
            items = field.get_items(pos)
            exploded, can_continue = True, True

            for item in items:
                if hasattr(item, 'explosion_hit'):
                    x, c = item.explosion_hit()
                    exploded = exploded and x
                    can_continue = can_continue and c

            if exploded:
                ex = Explosion(field.time + self.EXPLOSION_TIME)
                field.add_item(ex, pos)

            return can_continue

        def explode_direction(origin, direction):
            for i in range(self.EXPLOSION_RADIUS):
                pos = vadd(origin, vmul(direction, i + 1))
                if not in_bounds(pos, field.bounds):
                    return
                can_continue = explode_tile(pos)
                if not can_continue:
                    return

        explode_tile(origin)
        for d in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            explode_direction(origin, d)

    def explosion_hit(self):
        self.explode()
        return False, True


class Explosion(ActiveItem):
    """Explosion in progress"""

    def __init__(self, target_t=0):
        """The explosion will last until `target_t`"""
        self.target_t = target_t

    def update(self, t):
        if t >= self.target_t:
            self.field.remove_item(self)

    def explosion_hit(self):
        return False, True


class Wall(Item):
    """Impenetrable wall"""

    def explosion_hit(self):
        return False, False


class Brick(ActiveItem):
    """Bricks that can be destroyed"""

    def explosion_hit(self):
        self.field.remove_item(self)
        return True, False
