from .utils import quantize, vquantize, vadd, vmul, vsub, in_bounds


class Field:
    """Playing field where all items are and all the action happens

    Parameters:
    - width -- width of the field in tiles,
    - height -- height of the field in tiles,
    - ticks_per_second -- how many game ticks are one second,
    - pixels_per_unit -- determines mimimum distance (1 / pixels_per_unit).
    """

    def __init__(self, width, height, ticks_per_second=10, pixels_per_unit=10):
        self.width = width
        self.height = height
        self.bounds = ((0, 0), (self.width - 1, self.height - 1))
        self.ticks_per_second = ticks_per_second
        self.dt = 1 / self.ticks_per_second
        self.pixels_per_unit = pixels_per_unit
        self.time = 0
        self.items = []

    def quantize_pos(self, pos, to_tile=False):
        """Round position to minimum increment or to whole tile"""
        qpu = 1 if to_tile else self.pixels_per_unit
        return vquantize(pos, qpu)

    def add_item(self, item, pos):
        """Place item on the field in a specific position"""
        item.field = self
        item.pos = self.quantize_pos(pos)
        self.items.append(item)

    def remove_item(self, item):
        """Remove item from the field"""
        if item in self.items:
            self.items.remove(item)
            item.field = None
            item.pos = None

    def get_items(self, pos):
        """Return all items that intersect the tile at (row, col)"""
        ret = []
        for item in self.items:
            dx, dy = vsub(item.pos, pos)
            if -1 < dx < 1 and -1 < dy < 1:
                ret.append(item)
        return ret

    def _move_tick(self, pos, speed):
        """Calculate target position after one tick"""
        return self.quantize_pos(vadd(pos, vmul(speed, self.dt)))

    def _item_can_move_to(self, moving_item, pos):
        """Check that `pos` is not blocked by another item"""
        if not in_bounds(pos, self.bounds):
            return False

        for item in self.get_items(pos):
            if item != moving_item and item.blocking:
                return False

        return True

    def move_items(self):
        """Move items according to speeds, detect collisions, update speeds"""
        for item in self.items:
            if hasattr(item, 'speed') and item.speed != (0, 0):
                new_pos = self._move_tick(item.pos, item.speed)
                if self._item_can_move_to(item, new_pos):
                    item.pos = new_pos
                else:
                    item.speed = (0, 0)

    def tick(self):
        """Run the time into the future for one tick

        One tick equals `(1 / ticks_per_second)` seconds. Will update
        positions of all objects according to their speeds, handle collisions,
        process explosions and other events.
        """
        self.time = quantize(self.time + 1 / self.ticks_per_second,
                             self.ticks_per_second)
        for item in self.items:
            if hasattr(item, 'update'):
                item.update(self.time)
        self.move_items()
