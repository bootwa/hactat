# Directions
U = (0, -1)
UR = (1, -1)
R = (1, 0)
DR = (1, 1)
D = (0, 1)
DL = (-1, 1)
L = (-1, 0)
UL = (-1, -1)

DIRECTIONS = [U, UR, R, DR, D, DL, L, UL]

# Turn mappings
TURN_LEFT = {DIRECTIONS[i]: DIRECTIONS[i - 1] for i in range(8)}
TURN_RIGHT = {v: k for (k, v) in TURN_LEFT.items()}


def vmul(v, s):
    """Multiply 2-dimensional vector by a scalar"""
    return (v[0] * s, v[1] * s)


def vadd(v1, v2):
    """Add two 2-dimensional vectors"""
    return (v1[0] + v2[0], v1[1] + v2[1])


def vsub(v1, v2):
    """Subtract v2 from v1"""
    return (v1[0] - v2[0], v1[1] - v2[1])


def quantize(value, qpu):
    """Quantize the value to the grid of `qpu` parts per unit"""
    return round(value * qpu) / qpu


def vquantize(pos, qpu):
    """Quantize 2-vector to the grid of `qpu` parts per unit"""
    return (quantize(pos[0], qpu), quantize(pos[1], qpu))


def in_bounds(pos, bounds):
    """Check if `pos` is within `bounds`

    `pos` is a tuple of x and y coordinates and `bounds` is a tuple of two
    tuples that contain minium x and y and maximum x and y respectively:

        pos = (x, y)
        bounds = (min, max)
        min = (minx, miny)
        max = (maxx, maxy)
    """
    x, y = pos
    (x0, y0), (x1, y1) = bounds
    return x0 <= x <= x1 and y0 <= y <= y1


def vlen(vector):
    """Return vector length in maxabs norm"""
    return max(abs(vector[0]), abs(vector[1]))


def vnorm(vector):
    """Return normalized version of the `vector` (resized to `vlen = 1`)"""
    length = vlen(vector)
    if length == 0:
        return vector
    return vquantize(vmul(vector, 1 / length), 1)


def vturn(vector, angle):
    """Turn `vector` by `angle`

    Angle is in 1/8 of a turn (45 degrees). Turning preserves length of the
    vector in maxabs norm (`length = max(abs(x), abs(y)`).
    """
    length = vlen(vector)
    direction = vnorm(vector)

    if length == 0:
        return vector

    while angle > 0:
        direction = TURN_RIGHT[direction]
        angle -= 1
    while angle < 0:
        direction = TURN_LEFT[direction]
        angle += 1

    return vmul(direction, length)
